<nav id="scroll-ku" class="navbar navbar-white navbar-expand-lg fixed-top navbar-custom ">
		<div class="container">
			<a class="navbar-brand" href="#banner">
				<img src="/assets/images/logos/logo.png" alt="Logo" width="57" height="61"> Indopsiko Indonesia
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fa fa-align-right" style="color:white;"></i>
			</button>
				<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
					<a class="nav-link" href="/">Beranda</a>
					</li>
					<li class="nav-item">
					<a  class="nav-link" href="/profil">Tentang Kami</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/client">Klien</a>
					</li>
					<li class="nav-item">
						<a  class="nav-link" href="/service">Service</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/apply">Lowongan</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/news">Berita</a>
					</li>
					<li class="nav-item">
						<a class="nav-link"  href="/kontak">Kontak Kami</a>
					</li>
				</ul>
				</div>
		</div>
	  </nav>