

			<footer class="footer-area footer--light">
  <div class="footer-big">
    <!-- start .container -->
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-12">
          <div class="footer-widget">
            <div class="widget-about">
              <img src="/assets/images/logos/logo.png" alt="logo" width="30%" class="img-fluid">
              <p>Jalan Pahlawan Revolusi No.59, RT.6/RW.1, Pondok Bambu, Duren Sawit, RT.6/RW.1, Pd. Bambu, Kec. Duren Sawit, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13430</p>
              <ul class="contact-details">
                <li>
                  <span class="icon-earphones"></span> Call Us:
                  <a href="tel:(021)-8615604">(021)-8615604</a>
                </li>
                <li>
                  <span class="icon-envelope-open"></span>
                  <a href="mailto:support@indopsiko.com">support@indopsiko.com</a>
                </li>
              </ul>
            </div>
          </div>
          <!-- Ends: .footer-widget -->
        </div>
        <!-- end /.col-md-4 -->
        <div class="col-md-3 col-sm-4">
          <div class="footer-widget">
            <div class="footer-menu footer-menu--1">
              <h4 class="footer-widget-title">Link Terkait</h4>
              <ul>
                <li>
                  <a href="http://www.garudaescamas.com">Garuda Escammas</a>
                </li>
                <li>
                  <a href="https://ipl-express.com">IPL-Express</a>
                </li>
              </ul>
            </div>
            <!-- end /.footer-menu -->
          </div>
          <!-- Ends: .footer-widget -->
        </div>
        <!-- end /.col-md-3 -->

        <div class="col-md-3 col-sm-4">
          <div class="footer-widget">
            <div class="footer-menu">
              <h4 class="footer-widget-title">Sosial Media</h4>
              <ul>
                <li>
                  <a href="https://instagram.com/indopsiko.pt?igshid=ulwabie1oqde"><i class="fab fa-instagram"></i> @indopsiko</a>
                </li>
                <li>
                <a href="https://www.facebook.com/indopsiko.indonesia"><i class="fab fa-facebook"></i> @indopsiko</a>
                </li>
              </ul>
            </div>
            <!-- end /.footer-menu -->
          </div>
          <!-- Ends: .footer-widget -->
        </div>
        <!-- end /.col-lg-3 -->

        <div class="col-md-3 col-sm-4">
          <div class="footer-widget">
            <div class="footer-menu no-padding">
              <h4 class="footer-widget-title">Profil PT.Indopsiko</h4>
              <p>Pada tanggal 04 Maret 1994, Gunarto Gunotalijendro, SH, MM, mendirikan sebuah perusahaan yang bergerak dalam bidang Pengelolan dan Pemberdayaan Sumber Daya Manusia yakni PT. INDOPSIKO INDONESIA . <a href="/profil">Lihat Selengkapnya</a></p>
            </div>
            <!-- end /.footer-menu -->
          </div>
          <!-- Ends: .footer-widget -->
        </div>
        <!-- Ends: .col-lg-3 -->

      </div>
      <!-- end /.row -->
    </div>
    <!-- end /.container -->
  </div>
  <!-- end /.footer-big -->

  <div class="mini-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="copyright-text">
            <p>© 2020
              <a href="/">PT. Indopsiko Indonesia</a>. All rights reserved
            </p>
          </div>

          <div class="go_top">
            <a href="#"><span><i class="fa fa-arrow-up" style=color:white;></i></span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>