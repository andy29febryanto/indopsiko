@extends('beranda')
@section('title')
Indopsiko- Dashboard    
@endsection

@section('content')
<div class="container-fluid mt-5 pt-5">
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/">Beranda</a></li>
      <li class="breadcrumb-item active" aria-current="page">Service</li>
    </ol>
  </nav>
</div>

<div class="container-fluid mt-3 mb-3 bg-white">
    <div class="row text-center pt-3">
        <div class="col-md">
            <h2 class="wow bounce fast judul-profil" style="color:#42F0CD"><b>Our Service</b></h2>
        </div>
    </div>
    <div class="row text-center bg-white">
        <div class="col-md py-4">
            <div class="card text-center profil-banner wow bounce fast">
                <img class="card-img-top" src="/assets/images/bg/bg-8.jpg" alt="Card image" style="height:580px;">
                <div class="card-body">
                  <h4 class="card-title title-service">Kontraktor Tenaga Kerja</h4>
                  <p class="card-text text-justify">Sehubungan dengan Peraturan Menteri Tenaga Kerja Republik Indonesia NO.19 Tahun 2012 tentang SYARAT-SYARAT PENYERAHAN SEBAGIAN PELAKSANAAN PEKERJAAN KEPADA PIHAK LAIN dan Surat Edaran NO. SE-04/MEN/VIII/2013 tentang Pedoman Pelaksanannya, bersama ini kami PT. INDOPSIKO INDONESIA menawarkan kerjasama dalam PEMBORONGAN PEKERJAAN di perusahaan Bapak.
                    Karena berkembangnya pekerjaan perusahaan yang berakibat tidak memungkinkan masalah tenaga kerja ditangani oleh perusahaan sendiri, hal ini disebabkan oleh tuntutan globalisasi dan ketatnya persaingan sehingga perusahaan lebih berkonsentrasi kepada inovasi produk.</p>
                  </div>
              </div>
        </div>
    </div>
    <div class="row text-center bg-white">
        <div class="col-md py-4">
            <div class="card text-center profil-banner wow bounce fast">
                <img class="card-img-top" src="/assets/images/bg/bg-7.jpg" alt="Card image" style="height:580px;">
                <div class="card-body">
                  <h4 class="card-title title-service">Psikotest</h4>
                  <p class="card-text text-justify">Dalam menjalankan Psikotes Unjuk Kerja  Manageral PT. INDOPSIKO INDONESIA bekerjasama dengan GRAHITA INDONESIA suatu Lembaga Psikologi Terapan untuk melakukan Psikotes guna meningkatkan kwalitas Sumber Daya Manusia.
                    Psikotes kami memiliki Standard Internasional karena diciptakan oleh anak bangsa dengan study banding di Canada dan memiliki status Nasional karena telah mempunyai Hak Cipta yang di akui oleh Departemen Hukum dan Hak Asasi Manusia Repubik Indonesia tertanggal 4 Desember 2009.</p>
                  </div>
              </div>
        </div>
    </div>
    <div class="row text-center bg-white">
        <div class="col-md py-4">
            <div class="card text-center profil-banner wow bounce fast">
                <img class="card-img-top" src="/assets/images/bg/bg-6.jpg" alt="Card image" style="height:580px;">
                <div class="card-body">
                  <h4 class="card-title title-service">Motivation Training</h4>
                  <p class="card-text text-justify">Dengan adanya tuntutan globalisasi di era yang serba maju ini yang semuanya serba  cepat dan tepat di dalam persaingan yg semakin luas, sebagai manusia yang produktif pastilah mempunyai tantangan berat dalam menghadapi dunia modren. Untuk itu di perlukan Motivasi Kerja yang menghasil kan Good Skill, melalui training diharapkan peserta memahami, mengalami dan berlatih, sehingga Motivasi Kerja muncul secara reflek saat menghadapi permasalahan.
                    Peserta training akan  mendapatkan pengetahuan yang dapat memberikan kenyamanan dalam bekerja, sekaligus  akan menikmati hasil pekerjaanya.
                    Training Motivasi Kerja dikemas tidak sekedar teori tetapi juga disertai permainan ( game ) sehingga peserta  diharapkan berperan aktif</p>
                  </div>
              </div>
        </div>
    </div>
</div>
@endsection